# py-dough

py-dough는 파이썬으로 웹 서비스를 개발하기 위한 표준적인 프레임웍을 개발하는 프로젝트 이다.

이 프로젝트는 다음과 같은 내용을 포함한다.

* 코딩 규칙
* 웹 어플리케이션 보안 관리 방법
* 트랜잭션 관리 방법
* 데이터 접근 방법
* 사용자 권한 관리 방법
* RESTful 웹 서비스 구현 방법
* 웹 컨텐츠 서비스 구현 방법
* 배치 서비스 구현 방법
* 로깅 방법
* 패키징
* 단위 테스트 구현 방법